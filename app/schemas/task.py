from typing import List, Optional

from pydantic import BaseModel, Field

from app.schemas.post import PostWithTaskStatus


class Task(BaseModel):
    id: int
    status: str = Field(..., description="Статус задачи (назначена, в работе, успешно, провалено)")
    views_count: int = Field(None, description="Количество просмотров поста")

    class Config:
        orm_mode = True


class TaskPostsResponse(BaseModel):
    task_id: int
    posts: List[PostWithTaskStatus]

