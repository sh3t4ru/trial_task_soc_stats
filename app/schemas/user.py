from pydantic import BaseModel, Field


class UserCreate(BaseModel):
    username: str = Field(..., min_length=1)
    password: str = Field(..., min_length=4)


class ShowUser(BaseModel):
    id: int
    username: str

    class Config:
        orm_mode = True
