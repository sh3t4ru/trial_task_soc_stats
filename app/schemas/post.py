from typing import List, Union, Optional

from pydantic import BaseModel, HttpUrl


class PostCreate(BaseModel):
    link: HttpUrl
    task_id: int

    class Config:
        orm_mode = True


class PostTaskCreate(BaseModel):
    post_links: Union[HttpUrl, List[HttpUrl]]

    class Config:
        orm_mode = True


class PostWithTaskStatus(BaseModel):
    id: int
    link: str
    status: str
    views_count: Optional[int] = None
