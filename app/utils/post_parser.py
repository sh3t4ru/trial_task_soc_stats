import re
import requests
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup


def is_instagram_link(url: str):
    pattern = r'https?://(?:www\.)?instagram\.com/p/[A-Za-z0-9-_]+/?'
    return bool(re.match(pattern, url))


def is_vk_link(url: str):
    pattern = r'https?://(?:www\.)?vk\.com/wall-[A-Za-z0-9-_]+/?'
    return bool(re.match(pattern, url))


def collect_views_instagram(post_link: str):
    return len(post_link)  # TODO: stub


def collect_views_vk(post_link: str):
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.get(post_link)
    page = driver.page_source
    driver.quit()

    soup = BeautifulSoup(page, 'html.parser')

    # работает для постов вида https://vk.com/wall-72495085_1441435
    views_container = soup.find('div', class_='like_views like_views--inActionPanel')
    if views_container:
        return views_container.find('span', class_='_views').text\
            .replace('K', '000')\
            .replace('M', '000000')
    else:
        return -1


def collect_views(post_link: str):
    if is_instagram_link(post_link):
        return collect_views_instagram(post_link)
    if is_vk_link(post_link):
        return collect_views_vk(post_link)
    return -1
