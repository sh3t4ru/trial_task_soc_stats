from sqlalchemy.orm import Session

from app.db.repository.post import get_posts_by_task, update_post
from app.db.repository.task import update_task_status
from app.utils.post_parser import collect_views


def background_collect_views(task_id: int, db: Session):
    try:
        update_task_status(task_id=task_id, status="in progress", db=db)
        for post in get_posts_by_task(task_id=task_id, db=db):
            post.views_count = collect_views(post.link)
            update_post(post=post, db=db)
        update_task_status(task_id=task_id, status="success", db=db)
    except Exception as e:
        update_task_status(task_id=task_id, status="failed", db=db)
        print(e)
