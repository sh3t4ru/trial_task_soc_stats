from sqlalchemy.orm import Session

from app.core.hashing import Hasher
from app.db.models.user import User
from app.schemas.user import UserCreate


def create_new_user(user: UserCreate, db: Session):
    user = User(
        username=user.username,
        hashed_password=Hasher.get_password_hash(user.password),
    )
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def get_user(username: str, db: Session):
    user = db.query(User).filter(User.username == username).first()
    return user
