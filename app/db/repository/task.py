from sqlalchemy.orm import Session

from app.db.models.task import Task


def create_new_task(db: Session):
    new_task = Task(status="new")
    db.add(new_task)
    db.commit()
    db.refresh(new_task)

    return new_task


def update_task_status(task_id: int, status: str, db: Session):
    task = db.query(Task).filter(Task.id == task_id).first()
    task.status = status
    db.add(task)
    db.commit()

    return task



