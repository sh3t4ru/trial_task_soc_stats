from sqlalchemy.orm import Session

from app.db.models.post import Post
from app.schemas.post import PostCreate


def create_post(post: PostCreate, db: Session):
    existing_post = db.query(Post).filter(Post.link == post.link).first()
    if not existing_post:
        new_post = Post(link=post.link, task_id=post.task_id)
        db.add(new_post)
        db.commit()
        db.refresh(new_post)
        post_to_use = new_post
    else:
        existing_post.task_id = post.task_id
        db.add(existing_post)
        db.commit()
        post_to_use = existing_post

    return post_to_use


def update_post(post: Post, db: Session):
    existing_post = db.query(Post).filter(Post.id == post.id).first()
    if not existing_post:
        return
    existing_post.views_count = post.views_count
    existing_post.task_id = post.task_id
    db.add(existing_post)
    db.commit()
    return existing_post


def get_posts_by_task(task_id: int, db: Session):
    return db.query(Post).filter(Post.task_id == task_id).all()


def get_post_by_link(link: str, db: Session):
    return db.query(Post).filter(Post.link == link).first()
