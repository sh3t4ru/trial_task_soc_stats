from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Post(Base):
    id = Column(Integer, primary_key=True, index=True)
    link = Column(String, unique=True, index=True)
    views_count = Column(Integer, nullable=True)
    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship("Task", back_populates="posts")
