from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Task(Base):
    id = Column(Integer, primary_key=True, index=True)
    status = Column(String, default="new")
    posts = relationship("Post", back_populates="task")
