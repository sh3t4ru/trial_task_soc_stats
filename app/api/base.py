from fastapi import APIRouter

from app.api import route_login, route_user, route_post, route_task

api_router = APIRouter()
api_router.include_router(
    route_user.router, prefix="", tags=["users"]
)
api_router.include_router(
    route_login.router, prefix="", tags=["login"]
)
api_router.include_router(
    route_post.router, prefix="", tags=["posts"]
)
api_router.include_router(
    route_task.router, prefix="", tags=["tasks"]
)