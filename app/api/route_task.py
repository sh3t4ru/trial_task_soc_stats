from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app.api.route_login import get_current_user
from app.db.models.user import User
from app.db.repository.post import get_posts_by_task
from app.db.session import get_db
from app.schemas.post import PostWithTaskStatus
from app.schemas.task import TaskPostsResponse

router = APIRouter()


@router.get("/tasks/{task_id}/posts", response_model=TaskPostsResponse)
def get_task_posts(task_id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    posts = get_posts_by_task(task_id=task_id, db=db)
    if not posts:
        raise HTTPException(status_code=404, detail=f"No posts found for task with ID {task_id}")

    posts_with_status = []
    for post in posts:
        post_info = PostWithTaskStatus(
            id=post.id,
            link=post.link,
            status=post.task.status,
            views_count=post.views_count if post.task.status == "success" else None
        )
        posts_with_status.append(post_info)

    return TaskPostsResponse(task_id=task_id, posts=posts_with_status)
