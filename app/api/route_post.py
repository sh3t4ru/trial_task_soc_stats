
from fastapi import APIRouter, BackgroundTasks, Depends
from sqlalchemy.orm import Session

from app.db.repository.post import create_post
from app.db.repository.task import create_new_task, update_task_status
from app.db.session import get_db
from app.schemas.post import PostTaskCreate, PostCreate
from app.schemas.task import Task
from app.tasks.collect_views import background_collect_views

router = APIRouter()


@router.post("/collect", response_model=Task, status_code=201)
def collect_post_views(background_tasks: BackgroundTasks, post_task: PostTaskCreate, db: Session = Depends(get_db)):
    if isinstance(post_task.post_links, str):
        post_task.post_links = [post_task.post_links]

    task = create_new_task(db=db)
    for link in post_task.post_links:
        create_post(post=PostCreate(link=link, task_id=task.id), db=db)
    update_task_status(task_id=task.id, status="assigned", db=db)
    background_tasks.add_task(background_collect_views, task.id, db)

    return task