from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session

from app.db.repository.user import create_new_user
from app.db.session import get_db
from app.schemas.user import UserCreate, ShowUser

router = APIRouter()


@router.post("/", response_model=ShowUser, status_code=status.HTTP_201_CREATED)
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    user = create_new_user(user=user, db=db)
    return user
